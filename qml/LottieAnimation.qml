/*
 * Copyright (C) 2021  Michele Castellazzi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * rlottie-qml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QLottieFrameProvider 1.0
import QtMultimedia 5.12
import QtQuick 2.7

Item {
    id: lottieItem
    property alias source: frameProvider.source
    property alias play: frameProvider.play
    function next() {
        frameProvider.requestFrame()
    }

    QLottieFrameProvider {
        id: frameProvider
        property bool play: false
        width: videoOutput.width
        onWidthChanged: requestFrame(play)
        height: videoOutput.height
        onHeightChanged: requestFrame(play)
        onPlayChanged: if (play) { requestFrame() }
        onFramePresented: if (play) { requestFrame() }
    }

    VideoOutput {
        id: videoOutput
        source: frameProvider
        anchors.fill: parent
    }

    ShaderEffect {
        id: removeBackground
        anchors.fill: videoOutput
        property variant source: ShaderEffectSource { sourceItem: videoOutput; hideSource: true }

        fragmentShader: "
            varying highp vec2 qt_TexCoord0;
            uniform sampler2D source;
            uniform lowp float qt_Opacity;
            void main() {
                lowp vec4 sourceColor = texture2D(source, qt_TexCoord0);
                gl_FragColor = sourceColor * qt_Opacity;
        }"
    }
    Component.onCompleted: frameProvider.requestFrame()
}
